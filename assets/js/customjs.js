$(document).ready(function() {
	// document.oncontextmenu = function(){return false}

	var getaudio = $('#player')[0];
   /* Get the audio from the player (using the player's ID), the [0] is necessary */
   var mouseovertimer;
   /* Global variable for a timer. When the mouse is hovered over the speaker it will start playing after hovering for 1 second, if less than 1 second it won't play (incase you accidentally hover over the speaker) */
   var audiostatus = 'off';
   var site_url = 'http://zintroalum.azurewebsites.net/';
   
	$('nav a,#btn-ancla-contacto').on("click",function(e){	
		e.preventDefault();		//evitar el eventos del enlace normal
		var strAncla = $(this).attr('href'); //id del ancla
		$('body,html').stop(true,true).animate({				
		scrollTop: $(strAncla).offset({top:5000});
		},1000);	
	});

	$('.navbar-nav li a').click(function(e) {
		$('.navbar-nav li.active').removeClass('active');

		if (!$(this).parent().hasClass('active')) {
		 $(this).parent().addClass('active');
		}
		e.preventDefault();
	});

	$("#btn-enviar-correo").on('click', function() {
		$('#contact-form-zintroalum').validate({
		  ignore: ".ignore",
		  errorClass: "text-danger",
		  rules:{
		    contact_nombre_zintroalum:{required: true},
		    contact_mail_zintroalum:{required: true, email: true},
		    contact_distribuidor_zintroalum:{required: true},
		    contact_mensaje_zintroalum:{required: true},
		     hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
		    messages:{
			    contact_nombre_zintroalum:{required:"Campo requerido"},
			    contact_mail_zintroalum:{required:"Campo requerido"},
			    contact_distribuidor_zintroalum:{required:"Campo requerido"},
			    contact_mensaje_zintroalum:{required:"Campo requerido"},
			    hiddenRecaptcha:{required:"Campo reCaptcha requerido"}
			}
		  },submitHandler: function(argument) {
		      $.ajax({
		        url: site_url + 'Send_email/correo',
		        type: 'POST',
		        //cache: false,
		        dataType:"json",
		        data: $('#contact-form-zintroalum').serialize(),
		        beforeSend: function() {
				    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
				    $("#btn-enviar-correo").attr('disabled', 'disabled').html("Enviando...");
				},
                success:  function (e) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                	switch (e.status) {
			        	case "Error":
			        		$(".alert.msg-contacto").html("<strong>Alerta!</strong> "+e.msg).addClass('alert-danger');
			        		$("#btn-enviar-correo").attr('disabled', false).html("<b>ENVIAR</b> MENSAJE");
			        		break;
			        	case "OK":
			        		$(".alert.msg-contacto").html("<strong>Exito!</strong> "+e.msg).addClass('alert-success');
			        		$("#btn-enviar-correo").attr('disabled', false).html("<b>ENVIAR</b> MENSAJE");
			        		$("#contact_nombre_zintroalum").val("");
			        		$("#contact_mail_zintroalum").val("");
			        		$("#contact_mensaje_zintroalum").val("");
			        	break;
			        	default:
			        		console.log(e);
			        		break;
			        }
                }
		      }).done(function(e) {
		        console.log(e.status);
		        switch (e.status) {
		        	case "Error":
		        		$(".alert.msg-contacto").html("<strong>Alerta!</strong> "+e.msg).addClass('alert-danger');
		        		$("#btn-enviar-correo").attr('disabled', false).html("<b>ENVIAR</b> MENSAJE");
		        		break;
		        	case "OK":
		        	$(".alert.msg-contacto").html("<strong>Exito!</strong> "+e.msg).addClass('alert-success');
		        		$("#btn-enviar-correo").attr('disabled', false).html("<b>ENVIAR</b> MENSAJE");
		        	break;
		        	default:
		        		console.log(e);
		        		break;
		        }
		      }
		      );
		  }
		});
	});

	/*Audiuo control*/
	$(".btn-tone").on('click', function() {
		if (getaudio.paused == false) {
			getaudio.pause();
			$("#btn-tone").attr("src",site_url + "assets/img/ico-play-ringtone.png");
		} else{
	    	getaudio.play();
	    	$("#btn-tone").attr("src",site_url + "assets/img/ico-pausa-ringtone.png");
		}
	});
	/*Audiuo control*/
	
	/*Stop carousel*/
	$('#carouselExampleControls').carousel('pause');
	/*Stop carousel*/

	/*Responsive iFrames*/
	var $iframes = $( "iframe" );
	 
	// Find &#x26; save the aspect ratio for all iframes
	$iframes.each(function () {
	  $( this ).data( "ratio", this.height / this.width )
	    // Remove the hardcoded width &#x26; height attributes
	    .removeAttr( "width" )
	    .removeAttr( "height" );
	});
	 
	// Resize the iframes when the window is resized
	$( window ).resize( function () {
	  $iframes.each( function() {
	    // Get the parent container&#x27;s width
	    var width = $( this ).parent().width();
	    $( this ).width( width )
	      .height( width * $( this ).data( "ratio" ) );
	  });
	// Resize to fix all iframes on page load.
	}).resize();
});