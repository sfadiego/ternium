<div class="container"><!-- hidden-lg-up for movile-->
<br>
	<div class="row">
		<div class="col background-gray">
			<div class="row border-white-bottom padding-30-top padding-30-bottom">
				<div class="col-4 text-right">
					<h6 class="texto-rojo padding-top-25"><b>RESISTENTE <br>A LA <br>CORROSIÓN</b></h6>
				</div>
				<div class="col-8 padding-left-40 padding-right-40">
					<p class="texto-gris">El recubrimiento de Zinc y Aluminio previene la oxidación y prolonga la vida útil del producto.</p> 
				</div>
			</div>
			<div class="row border-white-bottom padding-30-top padding-30-bottom">
				<div class="col-4 text-right">
					<h6 class="texto-rojo padding-top-25"><b>APLICACIONES</b></h6>
				</div>
				<div class="col-8 padding-left-40 padding-right-40">
					<p class="texto-gris">Ideal para la instalación  de muros, techos y bardas en la industria de la construcción.</p>
				</div>
			</div>
			<div class="row border-white-bottom padding-30-top padding-30-bottom">
				<div class="col-4 text-right">
					<h6 class="texto-rojo padding-top-25"><b>GARANTÍA</b></h6>
				</div>
				<div class="col-8 padding-left-40 padding-right-40">
					<p class="texto-gris">Exige el certificado de calidad Ternium Zintro Alum.</p>
				</div>
			</div>				
		</div>
	</div>
</div>
<div class="container " >
<br>
	<div class="row">
		<div class="col-4 img-banner-inf-tecnica"></div>
		<div class="col-8 text-center section-informacion-tecnica">
			<div class="row">
				<div class="col-12 texto-blanco padding-20-top padding-40-bottom texto-recuerda">
					<b   class="grosor-400">Recuerda</b><br>
					<div class="grosor-100"> buscar el sello de calidad</div>
					<div class="grosor-100">Ternium Zintro Alum.</div>
				</div>
				<div class="col-12 texto-rojo sello-ternium-font-styles padding-bottom-60 texto-sello-ternium-movil">
					<span><p class="separador">¡Asegúrate </p> que tenga</span><br><span class="sello-ternium-bolder">el sello Ternium!</span>
				</div>	
				<div class="col-12">
					<div class="row texto-blanco">
						<div class="col">
							<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/guia_practica.pdf" target="_blank"><img style="width: 30px;" alt="Descarga la guía práctica" src="<?php echo base_url(); ?>assets/img/ico-descarga-guia.png"></a>
							<div class="texto-descargas">
								<!-- descarga la <br><b>guía práctica</b> -->
								<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/guia_practica.pdf" target="_blank"><br>Descarga la <br><b>guía práctica</b> de Aceros Recubiertos dónde podrás encontrar la gama completa de productos Ternium.</a>
							</div>
						</div>
						<div class="col">
							<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/informacion_tecnica.pdf" target="_blank"><img style="width: 30px;" alt="Descarga la información técnica" src="<?php echo base_url(); ?>assets/img/ico-informacion-tecnica.png"></a>
							<div class="texto-descargas"><br>
								<!-- descarga la <br><b>información técnica</b> -->
								<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/informacion_tecnica.pdf" target="_blank">descarga la <br><b>información técnica</b></a>
							</div>
						</div>
					</div>		
				</div>
			</div><br>
		</div>
	</div>
</div>
