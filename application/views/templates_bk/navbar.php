<nav class="navbar fixed-top bg-faded navbar-toggleable-md navbar-light padding-navbar-center">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Menú">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
      <img src="<?php echo base_url(); ?>assets/img/logo-ternium.png" style="/*width: 76px;*/" class="img-fluid" alt="ZINTROALUM">
    </a>

    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav nav-fill mx-auto w-100">
        <li class="nav-item active">
          <a class="nav-link" href="#top-page">INICIO</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#caracteristica-zintroalum">CARACTERÍSTICAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#video-zintroalum">VIDEOS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#tips-zintroalum">TIPS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#galeria-zintroalum">GALERÍA</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#contacto-zintroalum">CONTACTO</a>
        </li>
      </ul>
    </div>
</nav>
<div id="top-page"></div>