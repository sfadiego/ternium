<div class="container " id="video-zintroalum">
	<div class="row">
		<div class="col no-padding-left-right">
			<div id="carouselExampleIndicators2" class="carousel slide">
			  <ol class="carousel-indicators" style="bottom: 10px !important;">
			    <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
			    <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
			  </ol>

			  <div class="carousel-inner" role="listbox">
			    <div class="carousel-item active">
			      <iframe width="100%" height="600" src="https://www.youtube.com/embed/aAgla1kYUCs" frameborder="0" allowfullscreen></iframe>
			      
			    </div>
			    <div class="carousel-item">
			    	<iframe width="100%" height="600" src="https://www.youtube.com/embed/Qcy_fb5cSdc" frameborder="0" allowfullscreen></iframe>
			    	
			    </div>
			  </div>
			</div>
		</div>
	</div>		
</div>
