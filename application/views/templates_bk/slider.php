<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="<?php echo base_url(); ?>assets/img/zintroalum-el-mejor-acero-slider-2.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="display-3 text-shadow-black texto-slider44">El ACERO DE <br> <span class="texto-slider71">MÉXICO</span></h1>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="<?php echo base_url(); ?>assets/img/zintroalum-el-mejor-acero-slider-1.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="display-3 text-shadow-black texto-slider44">El ACERO DE <br> <span class="texto-slider71">MÉXICO</span></h1>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="<?php echo base_url(); ?>assets/img/zintroalum-el-mejor-acero-slider-4.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="display-3 text-shadow-black texto-slider44">El ACERO DE <br> <span class="texto-slider71">MÉXICO</span></h1>
      </div>
    </div>

    <div class="carousel-item">
      <iframe id="framevideo" width="100%" height="720" src="https://www.youtube.com/embed/aAgla1kYUCs?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
      <div class="carousel-caption d-none d-md-block"></div>
    </div>
  </div>
  <div class="container container-overlap-textslider hidden-md-down">
     <div class="row texto-blanco padding-bottom-60">
       <!-- <div class="col-12 padding-20-bottom">
          <h1 class="display-3 text-shadow-black texto-slider44">El ACERO DE <br> <span class="texto-slider71">MÉXICO</span></h1>
       </div> -->
       <div class="col-12 text-right" id="caracteristica-zintroalum">
          <h4 class="texto-gris-zintralum padding-10-bottom texto-interesado">Estoy interesado</h4>
         <a href="#contacto-zintroalum" id="btn-ancla-contacto" class="btn btn-danger background-red-zintroalum boton-contactanos"><b>¡CONTÁCTANOS</b> AHORA!</a>
       </div>
     </div>
    <div class="row section-car-overlap-slider" id="">
        <div class="col-4 text-center background-gray"><h5 class="texto-rojo titulo-caracteristicas"><b>RESISTENTE <br>A LA CORROSIÓN</b></h5></div>
        <div class="col-4 text-center background-gray border-color-gray"><h5 class="texto-rojo titulo-caracteristicas"><b>APLICACIONES</b></h5></div>
        <div class="col-4 text-center background-gray"><h5 class="texto-rojo titulo-caracteristicas"><b>GARANTÍA</b></h5></div>
    </div>
  </div>
</div>