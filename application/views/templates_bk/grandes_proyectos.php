<div class="container ">
	<div class="row padding-top-80 padding-20-bottom">
		<div class="col-3 text-center">
			<img class="img-fluid zintro-alum-icon-rojo" src="<?php echo base_url(); ?>assets/img/zintro-alum-icon.jpg" alt="" >
		</div>
		<div class="col-6 text-center texto-naraja-zintralum">
		 	<span class="rojo-24">&nbsp;</span>
			<h1 class="naranja-72"><b>HECHO</b></h1>
			<h2 class="naranja-57"><b>EN MÉXICO</b></h2>
		</div>
		<div class="col-3 "><br>
			<div class="negro-24">visita nuestro canal <br>en youtube</div>
			<a href="https://www.youtube.com/ternium" target="_blank"><img src="<?php echo base_url(); ?>assets/img/icono-youtube.png"></a>
		</div>
	</div>
</div>
<div class="container" id="tips-zintroalum" style="/*min-height: 700px;*/">
	<div class="row no-padding-left-right">
		<div class="col-4 background-brown">
			<div class="row grandes-proyectos-section-zintroalum">
				<div class="col-12 background-material-grandes-proyectos">
				</div>
			</div>
		</div>
		<div class="col-8 no-padding-left-right background-orange-zintroalum">
			<div class="row">
				<div class="col text-center padding-30-top padding-20-bottom">
					<h5>ALGUNOS <b>TIPS</b></h5>
				</div>
			</div>
		 	<div id="accordion" role="tablist" aria-multiselectable="true" class="custom-height-section">
		    
			    <div class="card">
				    <div class="card-header" role="tab" id="headingOne">
				      <h5 class="mb-0">
				        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">DESCARGA Y MANEJO</a>
				      </h5>
				    </div>

				    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
				      <div class="card-block" style="font-size: 0.8rem !important;">
				        <p>Cada paquete debe ser levantado por el centro de gravedad, de ser posible, los paquetes deberán permanecer fijos hasta su instalación final en la cubierta y deberán ser atados antes de levantarse.</p>
				        <p>Cuando los paquetes sean levantados con una grúa, deberán utilizarse bandas de nylon, un balancín y un barrote de madera como apoyo, nunca uses cables de acero porque dañarán las láminas.</p>
				        <p>Cuando se levanten los paquetes con un montacargas, las horquillas deberán estar separadas un mínimo de 1.50 metros. No transportes los paquetes abiertos y maneja despacio en terrenos irregulares para evitar que se doblen o marquen las láminas.</p>
				      </div>
				    </div>
				</div>

				<div class="card">
				    <div class="card-header" role="tab" id="headingTwo">
				      <h5 class="mb-0">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
				          ALMACENAMIENTO
				        </a>
				      </h5>
				    </div>
				    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
				      <div class="card-block" style="font-size: 0.8rem !important;">
				        <p>Los paquetes deben almacenarse sobre el piso, a una altura suficiente para permitir la circulación del aire por debajo y evitar que le entre agua, puedes evitarlo cubriéndolo con una lona impermeable, pero permitiendo la circulación del aire entre los dobleces de la lona y el piso.</p>
				        <p>Si no se van instalar las láminas de manera inmediata deben tomarse precauciones adicionales para protegerlas de la oxidación y los maltratos. Verifica que no se haya generado humedad en el interior de los paquetes durante la transportación, si detectas humedad, las láminas deben ser desempaquetadas para escurrirlas y secarlas, empacadas nuevamente y cubiertas ligeramente de forma que el aire pueda circular entre ellas.</p>
				        <p>No se recomienda el almacenamiento prolongado de láminas en paquetes.</p>
				      </div>
				    </div>
				</div>
				
				<div class="card">
				    <div class="card-header" role="tab" id="headingThree">
				      <h5 class="mb-0">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
				          EN OBRA EN PROCESO DE CONSTRUCIÓN
				        </a>
				      </h5>
				    </div>
				    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
				      <div class="card-block" style="font-size: 0.8rem !important;">
				        <p>Coloca los paquetes en las zonas más cercanas a su instalación, sobre una superficie firme y cuidándolos de los impactos o golpes que rayen o dañen el material. Es importante apoyar los paquetes con una pendiente para drenar el agua por lluvia, rocío o condensaciones.</p>
				        <p>Al terminar la jornada, es recomendable fijar los paquetes que se están utilizando para evitar que el viento vuele las hojas y las dañe o cause accidentes. Se pueden guardar los paquetes en almacenes, unos sobre otros hasta una altura de 1 metro, o en racks metálicos diseñados específicamente para este fin.</p>
				      </div>
				    </div>
				</div>
				  
				<div class="card">
				    <div class="card-header" role="tab" id="headingfour">
				      <h5 class="mb-0">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
				          LIMPIEZA Y MANTENIMIENTO AL TERMINAR LA INSTALACIÓN
				        </a>
				      </h5>
				    </div>

				    <div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour">
				      <div class="card-block" style="font-size: 0.8rem !important; height: 240px;overflow: scroll;">
				        <p>Una vez colocadas todas las piezas y molduras, debe realizarse una limpieza general de la superficie para evitar que los sobrantes que se originan al colocar las pijas o remaches le provoquen oxidación.</p>
				        <p>Se recomienda limpiar con brocha de pelo, cerdas suaves naturales o trapo suave semihúmedo las superficies donde se observen restos de partículas metálicas y/o algún otro sedimento. Polvo, aceite, grasa, huellas dactilares y cualquier otro contaminante deben ser removidos por completo al terminar la instalación para asegurar una vida útil adecuada en las superficies pintadas.</p>
				        <p>No instales láminas que estén en contacto con aceites o grasas y evita el contacto de estos materiales en las láminas instaladas.</p>
				        <p>Para quitar manchas severas y óxido provocado por partículas externas utiliza un limpiador para limpieza de tinas porcelanizadas, así como cera automotriz para el caso de partículas oxidadas. Es importante aplicar un enjuague profundo en las zonas en que hayas aplicado dicho limpiador. No deben utilizarse limpiadores abrasivos ni cepillo de alambre, ya que estos dañarían la superficie pintada, se recomienda probar primero en un área pequeña antes limpiar todo el producto.</p>
				        <p>Cuando sea requerida una pintura de retoque por una raspadura o maltrato de pintura del acabado de la lámina o sus molduras, puedes aplicar una capa de pintura de secado al aire, limpiando previamente la superficie afectada.</p>
				      </div>
				    </div>
				</div>

				<div class="card">
				    <div class="card-header" role="tab" id="headingfive">
				      <h5 class="mb-0">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
				          MANTENIMIENTO
				        </a>
				      </h5>
				    </div>

				    <div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive">
				      <div class="card-block" style="font-size: 0.8rem !important;">
				        <p>En caso de acumularse polvo sobre las superficies pintadas puede llegar a decolorar la pintura, sobre todo si se encuentra expuesta al sol por mucho tiempo, es por esto que se recomienda lavar cada año la lámina con agua a presión en forma de spray, ayudará a mantener la buena apariencia de los proyectos de construcción.</p>
				        <p>En áreas en donde existan depósitos de polvo acumulado, se puede aplicar una solución de detergente suave utilizando una esponja o trapo suave. Moja la superficie antes de limpiar y asegúrate de enjuagarla inmediatamente después, antes de que la solución se seque sobre la superficie.</p>
				        <p>Nunca utilices solventes para realizar trabajos de limpieza sobre la pintura de acabado.</p>
				      </div>
				    </div>
				  </div>

			</div>				
		</div>
	</div>
</div>