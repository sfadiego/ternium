    <footer class="footer">
        <div class="container">
            <div class="row textos-footer">
                <div class="col">
                    <div class="row">
                        
                        <div class="col-12"><br>
                            <i class="fa fa-facebook-square fa-2x"></i>
                            <i class="fa fa-twitter-square fa-2x"></i>
                            <i class="fa fa-youtube fa-2x"></i>
                        </div>
                    </div>
                </div>      
                <div class="col text-right">
                    <div class="row">
                        <div class="col-12">
                            Un <b>producto</b> de:
                        </div>
                        <div class="col-12">
                            <img src="<?php echo base_url(); ?>assets/img/logo-ternium.png" style="width: 76px;" class="img-fluid" alt="ZINTROALUM">
                        </div>
                        <div class="col-12">
                            © <?php echo date('Y'); ?> Ternium Zintro Alum. Todos los derechos reservados.
                        </div>
                    </div>
                </div>          
            </div> 

        </div> 
    </footer>
    
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<!-- <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script> -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script>
$(window).on('load',function() {
      // Animate loader off screen
      $(".se-pre-con").fadeOut("slow");;
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<!-- libreria validaciones  -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js" ></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js" ></script>
<script src="<?php echo base_url(); ?>assets/js/messages_es.js" ></script>

<script src="<?php echo base_url(); ?>assets/plugins/bootstrap4/js/bootstrap.min.js"></script>
<!-- desarrollo -->
<script src="<?php echo base_url(); ?>assets/js/customjs.js"></script>
<!-- produccion -->
<!-- <script src="<?php echo base_url(); ?>assets/js/customjs.min.js"></script> -->
<!-- captcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>