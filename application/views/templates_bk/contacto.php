<div class="container padding-top-60">
	<div class="row" id="contacto-zintroalum">
		<div class="col">
			<h1 class="texto-rojo texto57"><b>CONTÁCTANOS</b></h1>
		</div>
		<div class="col">
			<h4 class="texto-rojo texto30">¿TIENES ALGUNA <br><b>PREGUNTA Ó COMENTARIO?</b> </h4>
		</div>
	</div>
</div>
<div class="container padding-top-60 ">
	<!-- <div class="row"> -->
	<div class="alert msg-contacto" role="alert">
  		<span></span>
	</div>
		<form id="contact-form-zintroalum" class="row" name="contact-form-zintroalum">
		
			<div class="col">
				<label for="Nombre" class="labels-contacto"><b>Nombre:</b></label>
				<div class="form-group">
				  <input type="text" class="form-control" name="contact_nombre_zintroalum" id="contact_nombre_zintroalum" aria-describedby="sizing-addon2" placeholder="">
				</div>

				<label for="correo" class="labels-contacto"><b>Correo electrónico:</b></label>
				<div class="form-group">
				  <input type="text" class="form-control"  name="contact_mail_zintroalum" id="contact_mail_zintroalum"  aria-describedby="sizing-addon2">
				</div>
				<div class="form-group">
			    <label for="distribuidor" class="labels-contacto"><b>Contacto: </b></label>
			    <select class="form-control"  name="contact_distribuidor_zintroalum" id="contact_distribuidor_zintroalum">
			      <option value="">Seleccionar</option>
			      	<option value="Aguascalientes">Aguascalientes	</option>
					<option value="Baja_California">Baja California</option>
					<option value="Baja_California_Sur">Baja California Sur	</option>
					<option value="Campeche">Campeche</option>
					<option value="Chiapas">Chiapas	</option>
					<option value="Chihuahua">Chihuahua	</option>
					<option value="Ciudad_de_Mexico">Ciudad de México</option>
					<option value="Coahuila">Coahuila	</option>
					<option value="Colima">Colima	</option>
					<option value="Durango">Durango	</option>
					<option value="Estado_de_Mexico">Estado de México</option>
					<option value="Guanajuato">Guanajuato	</option>
					<option value="Guerrero">Guerrero	</option>
					<option value="Hidalgo">Hidalgo	</option>
					<option value="Jalisco">Jalisco	</option>
					<option value="Michoacán">Michoacán	</option>
					<option value="Morelos">Morelos	</option>
					<option value="Nayarit">Nayarit	</option>
					<option value="Nuevo_leon">Nuevo León	</option>
					<option value="Oaxaca">Oaxaca	</option>
					<option value="Puebla">Puebla	</option>
					<option value="Queretaro">Querétaro	</option>
					<option value="Quintana_Roo">Quintana Roo</option>
					<option value="San_Luis_Potosi">San Luis Potosí	</option>
					<option value="Sinaloa">Sinaloa	</option>
					<option value="Sonora">Sonora	</option>
					<option value="Tabasco">Tabasco	</option>
					<option value="Tamaulipas">Tamaulipas	</option>
					<option value="Tlaxcala">Tlaxcala	</option>
					<option value="Veracruz">Veracruz	</option>
					<option value="Yucatan">Yucatán	</option>
					<option value="Zacatecas">Zacatecas	</option>
			    </select>
			  </div>
			  <div class="form-group">
			  	<div class="g-recaptcha" data-sitekey="6LeYbR8UAAAAAH6ZZ1bZH5JP4zSnfvTtMq_N4mq5"></div>	
			  </div>
			</div>
			<div class="col">
				<div class="form-group">
				    <label for="exampleTextarea" class="labels-contacto"><b>Mensaje:</b></label>
				    <textarea class="form-control" name="contact_mensaje_zintroalum" id="contact_mensaje_zintroalum" rows="11"></textarea>
				  </div>
				  <button type="submit" id="btn-enviar-correo" class="btn btn-danger pull-right background-red-zintroalum boton-contactanos"><b>ENVIAR</b> MENSAJE</button>
				  
			</div>
		</form>
	<!-- </div> -->
</div>