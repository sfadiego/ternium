<div class="container" >
	<div class="row container-informacion-tecnica">
		<div class="col-6 img-banner-inf-tecnica"></div>
		<div class="col-6 text-center section-informacion-tecnica">
			<div class="row">
				<div class="col-12 texto-blanco espacio-texto-60-60 texto-recuerda">
					<b class="grosor-400">Recuerda</b><br>
					<div class="grosor-100"> buscar el sello de calidad</div>
					<div class="grosor-100">Ternium Zintro Alum.</div>
				</div>
				<div class="col-12 texto-rojo sello-ternium-font-styles padding-bottom-60 texto-sello-ternium">
					<span><p class="separador">¡Asegúrate </p> que tenga</span><br><span class="sello-ternium-bolder">el sello Ternium!</span>
				</div>	
				<div class="col-12">
					<div class="row texto-blanco">
						<div class="col">
							<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/guia_practica.pdf" target="_blank" title="Guía práctica Ternium Zintro Alum"><img alt="Guía práctica Ternium Zintro Alum" src="<?php echo base_url(); ?>assets/img/ico-descarga-guia.png"></a>
							<div class="texto-descargas">
								 <a href="http://mx.ternium.com/zintroalum/assets/ternium/files/guia_practica.pdf" target="_blank" title="Guía práctica Ternium Zintro Alum"><br>Descarga la <br><b>Guía Práctica</b>de Aceros Recubiertos dónde podrás encontrar la gama completa de productos Ternium.</a>
							</div>
						</div>
						<div class="col">
							<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/informacion_tecnica.pdf" target="_blank" title="Información técnica de Ternium Zintro Alum"><img alt="Información técnica de Ternium Zintro Alum" src="<?php echo base_url(); ?>assets/img/ico-informacion-tecnica.png"></a>
							<div class="texto-descargas">
							<br>
								<a href="http://mx.ternium.com/zintroalum/assets/ternium/files/informacion_tecnica.pdf" target="_blank" title="Información técnica de Ternium Zintro Alum">descarga la <br><b>información técnica</b></a>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
</div>
