
<div class="container" id="galeria-zintroalum">
<br><br>
	<div class="row">
		<div class="col-12">
			<h1 class="text-center texto-rojo texto-mejor-calidad"><b> MEJOR CALIDAD</b></h1>	
		</div>
		<div class="col-12">
			<h1 class="text-center texto-naraja-zintralum texto-mejor-calidad texto-comprobada"><b>COMPROBADA<b></b></h1>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col no-padding-left-right">
			<img class="img-fluid img-width-100" src="<?php echo base_url(); ?>assets/img/img-slider-calidad-1.jpg" alt="First slide"> 
		</div>
		<div class="col no-padding-left-right">
			<img class=" img-fluid img-width-100" src="<?php echo base_url(); ?>assets/img/img-slider-calidad-2.jpg" alt="Second slide"> 
		</div>
		<div class="col no-padding-left-right">
			<img class=" img-fluid img-width-100" src="<?php echo base_url(); ?>assets/img/zintroalum-galeria-4.jpg" alt="Tercer slide"> 
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-8 background-grisclaro-zintroalum padding-40-zintroalum">
			<div class="row padding-20-bottom">
				<h2 class="col texto-negro-claro-zintroalum">
					INFORMACIÓN TECNICA
				</h2>
			</div>
			<div class="row">
					<div id="carouselExampleControls" class="carousel slide" data-ride="false">
					  <div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img class="d-block img-fluid" src="<?php echo base_url(); ?>assets/img/informacion-tecnica-1.png" alt="First slide">
					    </div>
					    <div class="carousel-item">
					      <img class="d-block img-fluid" src="<?php echo base_url(); ?>assets/img/informacion-tecnica-2.png" alt="Second slide">
					    </div>
					  </div>
					  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
					</div>
			</div>
			<div class="row padding-30-top">
				
			</div>	
		</div>
		<div class="col-4 padding-40-zintroalum text-center background-brown">
			<div class="row texto-blanco">
				<br><br>
				<h3 class="col-12"><b class="texto-descarga-ringtone">Descarga</b></h3>
				<h5 class="col-12 texto-ringtone">el ringtone de Los Tres<br> Tristes Tigres</h5>
				<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>
			</div>
			<div class="row texto-blanco">
				<span class="col-12 texto-sonido">Lleva el sonido de nuestro <br>jingle en tu celular. </span>
			</div><br>
			<div class="row">
				<span class="col-12 texto-sonido-naranja">¡Escúchalo y descárgalo aquí <br>mismo!</span>
			</div>
			<br><br>
			<div class="row">
				<div class="col texto-blanco">
						<div class="speaker">
							<img id="btn-tone" style="cursor: pointer;" alt="Escucha el ringtone de los Tres Tristes Tigres" src="<?php echo base_url(); ?>assets/img/ico-play-ringtone.png">
						</div>
						<audio id="player" src="<?php echo base_url(); ?>assets/audio/jingle.mp3" type="audio/mp3"></audio>
				</div>
				
				<div class="col texto-blanco">
					<a target="_blank" class="texto-reproduce" href="<?php echo base_url(); ?>assets/audio/jingle.mp3">
						<img alt="Ponle Tú Sello y descarga el ringtone de los Tres Tristes Tigres" src="<?php echo base_url(); ?>assets/img/ico-download-ringtone.png"></a>
				</div>
			</div>
			<div class="row">
				<div class="col texto-blanco text-center texto-reproduce">
					<span>Reproducir</span>
				</div>
				<div class="col texto-blanco text-center">
					<a target="_blank" class="texto-reproduce" href="<?php echo base_url(); ?>assets/audio/jingle.mp3">Descargar</a>
				</div>

			</div><br><br>
			<div class="row position-bottom-img">
				<div class="col text-center">
					<img src="<?php echo base_url(); ?>assets/img/zintralum-tres-tristres-img.jpg" class="img-fluid" alt="">
				</div>
				
			</div>
		</div>
	</div> -->		
</div>