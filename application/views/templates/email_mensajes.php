<p>La persona <strong><?php echo utf8_decode($nombre);?></strong> ha enviado un mensaje desde la forma de contacto, de igual forma indic&oacute; que el punto m&aacute;s cercano para ella es: <b><?php echo utf8_decode($distribuidor);?></b>. </p>

<p>A continuaci&oacute;n se presenta el mensaje que envi&oacute;:</p>
<strong>Nombre:</strong> <?php echo utf8_decode($nombre);?>.<br>
<strong>Correo electr&oacute;nico:</strong> <?php echo $email;?>.<br>
<strong>Distribuidor:</strong> <?php echo utf8_decode($distribuidor);?>.
<p><strong>Mensaje:</strong><br><?php echo utf8_decode($mensaje);?></p>