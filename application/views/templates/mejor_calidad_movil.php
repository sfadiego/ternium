<div class="container">
<br><br>
	<div class="row">
		<div class="col-12">
			<h1 class="text-center texto-rojo texto-mejor-calidad"><b> MEJOR CALIDAD</b></h1>	
		</div>
		<div class="col-12">
			<h1 class="text-center texto-rojo texto-mejor-calidad texto-comprobada"><b>COMPROBADA<b></b></h1>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-4 img-banner-inf-tecnica"></div>
		<div class="col-8 text-center section-informacion-tecnica">
			<div class="row texto-blanco">
				
				<h3 class="col-12"><br><b class="texto-descarga-ringtone">Descarga</b></h3>
				<h5 class="col-12 texto-ringtone">el ringtone de los <b style="font-weight: 600;"><br>Tres Tristes Tigres</b></h5>
				<br>
			</div>
			<div class="row texto-blanco">
				<span class="col-12 texto-sonido">Lleva el sonido de nuestro jingle en tu celular. </span>
			</div><br>
			<div class="row">
				<span class="col-12 texto-sonido-naranja">¡Escúchalo y descárgalo aquí <br>mismo!</span>
			</div>
			<br><br>
			<div class="row text-center">
				<div class="col-12 texto-blanco">
						<div class="speaker">
							<img style="width: 64px;" id="btn-tone" class="btn-tone" alt="Ternium Zintro Alum" src="<?php echo base_url(); ?>assets/img/ico-play-ringtone.png">
						</div>
						<div class="col text-center texto-reproduce"><span>Reproducir</span></div> 
						<audio id="player" src="<?php echo base_url();?>assets/audio/jingle.mp3" type="audio/mp3"></audio>

				</div>
				<div class="col-12 texto-blanco">
					<img style="width: 64px;" alt="Ternium Zintro Alum" src="<?php echo base_url();?>assets/img/ico-download-ringtone.png"><br>
					<a target="_blank" class="texto-reproduce" href="<?php echo base_url();?>assets/audio/jingle.mp3">Descargar</a><br><br>
				</div>
			</div> 
		</div>
	</div>
</div>
 