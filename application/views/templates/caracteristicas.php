<div class="container ">
	<div class="row section-caracteristicas-container">
		<div class="col text-center background-gray">
			<p class="texto-gris">Zintro Alum es ideal para la instalación de muros, techos y bardas en la industria de la construcción.</p>
		</div>
		<div class="col text-center background-gray border-color-gray">
			<p class="texto-gris">Zintro Alum es resistente, fresco y durable</p>
		</div>
		<div class="col text-center background-gray">
			<p class="texto-gris">Exige el certificado de calidad Ternium Zintro Alum.</p>
		</div>
	</div>
</div><br><br>