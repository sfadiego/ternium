<div class="container" >
	<br><br>
	<div class="row">
		<div class="col-12">
			<h1 class="text-center texto-rojo texto-mejor-calidad"><b> MEJOR CALIDAD</b></h1>	
		</div>
		<div class="col-12">
			<h1 class="text-center texto-rojo texto-mejor-calidad texto-comprobada"><b>COMPROBADA<b></b></h1>
		</div>
	</div>
	<br>
	<div class="row container-informacion-tecnica">
		<div class="col-6 img-banner-inf-tecnica"></div>
		<div class="col-6 text-center section-informacion-tecnica">
			<div class="row texto-blanco">
				
				<h3 class="col-12"><br><b class="texto-descarga-ringtone">Descarga</b></h3>
				<h5 class="col-12 texto-ringtone">el ringtone de <br>Los Tres Tristes Tigres</h5>
				
				<div class="custom-space-120"></div>
			</div>
			<div class="row texto-blanco">
				<span class="col-12 texto-sonido">Lleva el sonido de nuestro <br>jingle en tu celular. </span>
			</div><br>
			<div class="row">
				<span class="col-12 texto-sonido-naranja">¡Escúchalo y descárgalo aquí <br>mismo!</span>
			</div>
			<br><br>
			<div class="row custom-space-100">
				<div class="col-6 texto-blanco">
						<div class="speaker">
							<img id="btn-tone" class="btn-tone" style="cursor: pointer;" alt="Ternium Zintro Alum" src="<?php echo base_url();?>assets/img/ico-play-ringtone.png">
						</div>
						<audio id="player" src="<?php echo base_url();?>assets/audio/jingle.mp3" type="audio/mp3"></audio>
						<div class="col-12">
							<div class=" texto-blanco text-center texto-reproduce"><br>
								<span class="btn-tone" style="cursor: pointer;">Reproducir</span>
							</div><br><br><br>
						</div>
				</div>
				
				<div class="col-6 texto-blanco">
					<a target="_blank" class="texto-reproduce" href="<?php echo base_url(); ?>assets/audio/jingle.mp3">
						<img style="width:64px; " alt="Ternium Zintro Alum" src="<?php echo base_url();?>assets/img/ico-download-ringtone.png">
					</a>
					<div class="col-12">
						<div class="texto-blanco text-center"><br>
							<a target="_blank" class="texto-reproduce" href="<?php echo base_url();?>assets/audio/jingle.mp3">Descargar</a>
						</div>
					</div>
				</div>
				
			</div>
			<br><br>
			<div class="col-12 position-bottom-img">
				<div class="col text-center">
					<img src="<?php echo base_url();?>assets/img/zintralum-tres-tristres-img.png" class="img-fluid" alt="Ternium Zintro Alum">
				</div>
			</div>
		</div>
	</div>
</div>
