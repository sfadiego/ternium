
<div class="container" id="caracteristica-zintroalum" >
	<div class="row">
		<div class="col-8 background-grisclaro-zintroalum padding-40-zintroalum">
			<div class="row padding-20-bottom">
				<h3 class="col texto-negro-claro-zintroalum">
					INFORMACIÓN TÉCNICA TERNIUM ZINTRO ALUM
				</h3>
			</div>
			<div class="row">
					<div id="carouselExampleControls" class="carousel slide" data-ride="false">
					  <div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img class="d-block img-fluid" src="<?php echo base_url();?>assets/img/informacion-tecnica-1.png" alt="Ternium Zintro Alum">
					    </div>
					    <div class="carousel-item">
					      <img class="d-block img-fluid" src="<?php echo base_url();?>assets/img/informacion-tecnica-2.png" alt="Ternium Zintro Alum">
					    </div>
					  </div>
					  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
					</div>
			</div>
			<div class="row padding-30-top"></div>	
		</div>
		<div class="col-4 padding-40-zintroalum text-center background-brown">
			<div class="row">
				<div class="col-12 texto-blanco espacio-texto-60-60 texto-recuerda">
					<!-- <b class="grosor-400">Recuerda</b><br> -->
					<!-- <div class="grosor-100"> buscar el sello de calidad</div> -->
					<div class="grosor-100">Ternium Zintro Alum.</div>
				</div>
				<div class="col-12 texto-rojo sello-ternium-font-styles padding-bottom-60 texto-sello-ternium">
					<span><p class="separador">¡Asegúrate </p> que tenga</span><br><span class="sello-ternium-bolder">el sello Ternium!</span>
				</div>	
				<div class="col-12">
					<div class="row texto-blanco">
						<div class="col-12">
							<a href="<?php echo base_url(); ?>assets/files/guia_practica.pdf" target="_blank" title="Guía práctica Ternium Zintro Alum"><img alt="Guía práctica Ternium Zintro Alum" src="<?php echo base_url();?>assets/img/ico-descarga-guia.png"></a>
							<div class="texto-descargas">
								 <a href="<?php echo base_url(); ?>assets/files/guia_practica.pdf" target="_blank" title="Guía práctica Ternium Zintro Alum"><br>Descarga la <br><b>Guía Práctica</b> de Aceros Recubiertos dónde podrás encontrar la gama completa de productos Ternium.</a>
							</div><br><br>
						</div>
						<div class="col-12 info-tecnica-espacio">
							<a href="<?php echo base_url(); ?>assets/files/informacion_tecnica.pdf" target="_blank" title="Información técnica de Ternium Zintro Alum"><img alt="Información técnica de Ternium Zintro Alum" src="<?php echo base_url();?>assets/img/ico-informacion-tecnica.png"></a>
							<div class="texto-descargas">
							<br>
								<a href="<?php echo base_url(); ?>assets/files/informacion_tecnica.pdf" target="_blank" title="Información técnica de Ternium Zintro Alum">Descarga la <br><b>información técnica</b></a>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>		
</div>