    <footer class="footer">
        <div class="container">
            <div class="row textos-footer">
                <div class="col">
                    <div class="row">
                        <div class="col-12"><b>Siguenos en:</b><br>
                            <a target="_blank" href="https://www.facebook.com/TerniumenMonterrey/" title=""><i style="padding-right: 10px" class="fa fa-facebook-square fa-2x"></i></a>
                            <a target="_blank" href="https://twitter.com/terniummexico?lang=es" title=""><i style="padding-right: 10px" class="fa fa-twitter-square fa-2x"></i></a>
                            <a target="_blank" href="https://www.youtube.com/user/ternium" title=""><i style="padding-right: 10px" class="fa fa-youtube-play fa-2x"></i></a>
                            <a target="_blank" href="https://www.instagram.com/aceroternium/" title=""><i style="padding-right: 10px" class="fa fa-instagram fa-2x"></i></a>
                        </div>
                    </div>
                </div>      
                <div class="col-12 col-md-6 text-right">
                    <div class="row">
                        <div class="col-12">
                            Un <b>producto</b> de:
                        </div>
                        <div class="col-12">
                            <img src="<?php echo base_url();?>assets/img/logo-ternium.png" style="width: 76px;" class="img-fluid" alt="Ternium Zintro Alum">
                        </div>
                        <div class="col-12">
                            © <?php echo date('Y'); ?> Ternium Zintro Alum. Todos los derechos reservados. <a target="_blank" href="http://mx.ternium.com/aviso-de-privacidad/?lang=es" title="AViso de privacidad">Aviso de privacidad</a>
                        </div>
                    </div>
                </div>          
            </div> 

        </div> 
    </footer>
<script src="<?php echo base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
<script>$(window).on('load',function() {$(".se-pre-con").fadeOut("slow");});</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous" type="text/javascript"></script>
<script async src="<?php echo base_url();?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script async src="<?php echo base_url();?>assets/js/additional-methods.min.js" type="text/javascript"></script>
<script async src="<?php echo base_url();?>assets/js/messages_es.js" type="text/javascript"></script>
<script async src="<?php echo base_url();?>assets/plugins/bootstrap4/js/bootstrap.min.js" type="text/javascript"></script>
<script async src="<?php echo base_url();?>assets/js/customjs.min.js" type="text/javascript"></script>
<script async src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-592706dae30178a5"></script> 
