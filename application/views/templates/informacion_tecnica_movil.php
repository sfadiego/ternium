<div class="container" id="caracteristica-zintroalum">
	<div class="row">
		<div class="col-12 background-grisclaro-zintroalum padding-40-zintroalum">
			<div class="row padding-20-bottom">
				<div class="col">
					<h5 class="texto-negro-claro-zintroalum text-center">
						INFORMACIÓN TECNICA TERNIUM ZINTRO ALUM
					</h5>
				</div>
			</div>
			<div class="row"> <br>
				<div id="carouselExampleControls" class="carousel slide" data-ride="false">
					  <div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img class="d-block img-fluid" src="<?php echo base_url();?>assets/img/informacion-tecnica-1.png" alt="Ternium Zintro Alum">
					    </div>
					    <div class="carousel-item">
					      <img class="d-block img-fluid" src="<?php echo base_url();?>assets/img/informacion-tecnica-2.png" alt="Ternium Zintro Alum">
					    </div>
					  </div>
					  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
				</div>
			</div>
			<div class="row padding-30-top"></div>	
		</div>
		<div class="col-12 padding-40-zintroalum text-center background-brown">
			<div class="row">
				<div class="col-12 texto-blanco padding-20-top padding-40-bottom texto-recuerda-movil">
					<!-- <b   class="grosor-400">Recuerda</b><br> -->
					<!-- <div class="grosor-100"> buscar el sello de calidad</div> -->
					<div class="grosor-100">Ternium Zintro Alum.</div>
				</div>
				<div class="col-12 texto-rojo sello-ternium-font-styles padding-bottom-60 texto-sello-ternium-movil">
					<span><p class="separador">¡Asegúrate </p> que tenga</span><br><span class="sello-ternium-bolder">el sello Ternium!</span>
				</div>	
				<div class="col-12">
					<div class="row texto-blanco">
						<div class="col">
							<a href="<?php echo base_url(); ?>assets/files/guia_practica.pdf" target="_blank"><img style="width: 30px;" alt="Descarga la guía práctica" src="<?php echo base_url();?>assets/img/ico-descarga-guia.png"></a>
							<div class="texto-descargas">
								
								<a href="<?php echo base_url(); ?>assets/files/guia_practica.pdf" target="_blank"><br>Descarga la <br><b>guía práctica</b> de Aceros Recubiertos dónde podrás encontrar la gama completa de productos Ternium.</a>
							</div>
						</div>
						<div class="col">
							<a href="<?php echo base_url(); ?>assets/files/informacion_tecnica.pdf" target="_blank"><img style="width: 30px;" alt="Descarga la información técnica" src="<?php echo base_url();?>assets/img/ico-informacion-tecnica.png"></a>
							<div class="texto-descargas"><br>
								
								<a href="<?php echo base_url(); ?>assets/files/informacion_tecnica.pdf" target="_blank">Descarga la <br><b>información técnica</b></a>
							</div>
						</div>
					</div>		
				</div>
			</div><br>
		</div>
	</div>
</div>
