<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>

  <div class="carousel-inner" role="listbox" id="que-es-zintroalum">
    <div class="carousel-item active">
      <div class="fill" style="background-image:url('<?php echo base_url();?>assets/img/zintroalum-el-mejor-acero-slider-2.jpg');background-size:cover;background-repeat:no-repeat;"></div>
      <div class="carousel-caption d-none d-md-block">
        <h1 class="display-3 text-shadow-black texto-slider44">¿QUÉ ES TERNIUM ZINTRO ALUM? </h1>
        <h3 class="texto-interesado">Lámina fabricada con acero recubierto de zinc y <br>aluminio que previene la oxidación y <br>prolonga su vida útil.</h3>
      </div>
    </div>
    <div class="carousel-item ">
      <div class="fill" style="background-image:url('<?php echo base_url();?>assets/img/zintroalum-el-mejor-acero-slider-1.jpg');background-size:cover;background-repeat:no-repeat;"></div>
      <div class="carousel-caption d-none d-md-block">
        <h1 class="display-3 text-shadow-black texto-slider44">¿QUÉ ES TERNIUM ZINTRO ALUM? </h1>
        <h3 class="texto-interesado">Lámina acanalada de gran uso para la construcción <br>de techos y muros.</h3>

      </div>
    </div>
    <div class="carousel-item ">
      <div class="fill" style="background-image:url('<?php echo base_url();?>assets/img/zintroalum-el-mejor-acero-slider-4.jpg'); background-size:cover;background-repeat:no-repeat;"></div>
      <div class="carousel-caption d-none d-md-block">
        <h1 class="display-3 text-shadow-black texto-slider44">¿QUÉ ES TERNIUM ZINTRO ALUM? </h1>
        <h3 class="texto-interesado">Lámina recubierta con zinc y aluminio, para la <br>industria de la construcción.</h3>
      </div>
    </div>
    
  </div>
  
  <!-- hidden-md-down -->
  <div id="" class="container container-overlap-textslider">
     <div class="row texto-blanco padding-bottom-60">
       <div class="col-12 text-right" id="">
          <h4 class="texto-gris-zintralum padding-10-bottom texto-interesado">Estoy interesado</h4>
         <a href="#contacto-zintroalum" id="btn-ancla-contacto" class="btn btn-danger background-red-zintroalum boton-contactanos"><b>¡CONTÁCTANOS</b> AHORA!</a>
       </div>
     </div>
    <div class="row section-car-overlap-slider" id="">
        <div class="col-4 text-center background-gray"><h5 class="texto-rojo titulo-caracteristicas"><b>USOS</b></h5></div>
        <div class="col-4 text-center background-gray border-color-gray"><h5 class="texto-rojo titulo-caracteristicas"><b>BENEFICIOS</b></h5></div>
        <div class="col-4 text-center background-gray"><h5 class="texto-rojo titulo-caracteristicas"><b>GARANTÍA</b></h5></div>
    </div>
  </div>
</div>