<div class="container" id="que-es-zintroalum">
<br>
	<div class="row">
		<div class="col background-gray">
			<div class="row border-white-bottom padding-30-top padding-30-bottom">
				<div class="col-4 text-right">
					<h6 class="texto-rojo padding-top-25"><b>USOS</b></h6>
				</div>
				<div class="col-8 padding-left-40 padding-right-40">
					<p class="texto-gris">Zintro Alum es ideal para la instalación de muros, techos y bardas en la industria de la construcción.</p> 
				</div>
			</div>
			<div class="row border-white-bottom padding-30-top padding-30-bottom">
				<div class="col-4 text-right">
					<h6 class="texto-rojo padding-top-25"><b>BENEFICIOS</b></h6>
				</div>
				<div class="col-8 padding-left-40 padding-right-40">
					<p class="texto-gris">Zintro Alum es resistente, fresco y durable.</p>
				</div>
			</div>
			<div class="row border-white-bottom padding-30-top padding-30-bottom">
				<div class="col-4 text-right">
					<h6 class="texto-rojo padding-top-25"><b>GARANTÍA</b></h6>
				</div>
				<div class="col-8 padding-left-40 padding-right-40">
					<p class="texto-gris">Exige el certificado de calidad Ternium Zintro Alum.</p>
				</div>
			</div>				
		</div>
	</div>
</div>
<style type="text/css">.row.section-car-overlap-slider{display: none;}</style>
<br><br>