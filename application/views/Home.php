<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="cache-control" content="max-age=1296000" />
    <meta charset="utf-8">
    <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>TERNIUM ZINTRO ALUM</title>
    <meta name="description" content="Ternium Zintro Alum ¡Hecho en México! Lámina Galvanizada para techos y muros ¡Asegúrate que tenga el Sello Ternium! Cercanía, Experiencia y Calidad en el Acero.">
    <style>
      @import url('https://fonts.googleapis.com/css?family=Roboto');
      @import url('https://fonts.googleapis.com/css?family=Raleway:400,700');
    </style>
    <style type="text/css">
      .no-js #loader { display: none;  }
      .js #loader { display: block; position: absolute; left: 100px; top: 0; }
      .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('assets/img/preloader-zintroalum.gif') center no-repeat #fff;
      }
    </style>
    <!-- <link rel="preload" href="<?php //echo base_url();?>assets/plugins/bootstrap4/css/bootstrap.min.css" as="style" onload="this.rel='stylesheet'"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/bootstrap4/css/bootstrap.min.css">
    

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script type="text/javascript">
      WebFontConfig = {
        google: { families: [ 'Roboto+Condensed:400,700:latin', 'Source+Sans+Pro:400,600,400italic,700italic,700:latin' ] }
      };
      (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
          '://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })(); 
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-99018940-1', 'auto'); /* <============ INDICAR LA CUENTA (ID) DE GOOGLE ANALYTICS CREADO PARA LA LANDING PAGE */
      ga('send', 'pageview');

    </script>
    <!-- <link rel="preload" href="<?php echo base_url();?>assets/css/layoutb4.css" as="style" onload="this.rel='stylesheet'">  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/layoutb4.css" > 
    <!-- <link rel="preload" href="<?php echo base_url();?>assets/css/layoutb4.min.css" as="style" onload="this.rel='stylesheet'">  -->
    
    <script>
    !function(a){"use strict";var b=function(b,c,d){function e(a){return h.body?a():void setTimeout(function(){e(a)})}function f(){i.addEventListener&&i.removeEventListener("load",f),i.media=d||"all"}var g,h=a.document,i=h.createElement("link");if(c)g=c;else{var j=(h.body||h.getElementsByTagName("head")[0]).childNodes;g=j[j.length-1]}var k=h.styleSheets;i.rel="stylesheet",i.href=b,i.media="only x",e(function(){g.parentNode.insertBefore(i,c?g:g.nextSibling)});var l=function(a){for(var b=i.href,c=k.length;c--;)if(k[c].href===b)return a();setTimeout(function(){l(a)})};return i.addEventListener&&i.addEventListener("load",f),i.onloadcssdefined=l,l(f),i};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);
    /*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License */
    !function(a){if(a.loadCSS){var b=loadCSS.relpreload={};if(b.support=function(){try{return a.document.createElement("link").relList.supports("preload")}catch(b){return!1}},b.poly=function(){for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d,d.getAttribute("media")),d.rel=null)}},!b.support()){b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){b.poly(),a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){a.clearInterval(c)})}}}(this);
    </script>
  </head>
  <body>
  <div class="se-pre-con"></div>
  <?php echo $navbar ?>
  <?php echo $slider ?>
  <?php echo $caracteristica; ?>
  <?php //echo $sec_galeria; ?>
  <?php echo $informacion_tecnica; ?>
  <?php echo $sec_video; ?>
  <?php echo $sec_tips; ?>
  <?php echo $mejor_calidad; ?>
  <?php echo $contacto; ?>
  <?php echo $footer ?>
  <br><br><br>
  </body>
</html>