<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Send_email extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
    }

	public function correo() {
		
		if($this->input->post()) {

			$this->form_validation->set_rules('contact_nombre_zintroalum', '<b>Nombre</b>', 'trim|required');
			$this->form_validation->set_rules('contact_mail_zintroalum', '<b>Correo</b>', 'trim|required');
			$this->form_validation->set_rules('contact_mensaje_zintroalum', '<b>Mensaje</b>', 'trim|required');


			if ($this->form_validation->run() == FALSE) {
				$msg['status']='Error';
		        $msg['msg']=validation_errors();
			}
			else {
				if ($this->input->post('g-recaptcha-response') == FALSE) {
					$msg['status']='Error';
					$msg['msg'] = "Ha ocurrido un error, verificación recaptcha obligatorio";
				}else{
					$nombre=trim($this->input->post('contact_nombre_zintroalum'));
					$email=trim($this->input->post('contact_mail_zintroalum'));
					$distribuidor= trim($this->input->post('contact_distribuidor_zintroalum'));
					$mensaje=trim($this->input->post('contact_mensaje_zintroalum'));

					$config = array(
					    'protocol' => 'smtp',
					    'smtp_host' => 'smtp.sendgrid.net',
					    'smtp_port' => 587,
					    'smtp_user' => 'USUARIO-SENDGRID-DE-AZURE@azure.com',  #<============ INDICAR LA CUENTA DE CORREO DE SENDGRID DE AZURE
					    'smtp_pass' => 'CONTRASEÑA',						   #<============ INDICAR LA CONTRASEÑA DEL CORREO DE SENDGRID DE AZURE
					    'mailtype'  => 'html', 
					    'charset'   => 'iso-8859-1',
					    'wordwrap' => true,
					    'crlf' => "\r\n",
			  			'newline' => "\r\n"
					);

					$this->email->initialize($config);
			     
			        $this->email->from('comunicaciones@ternium.com.mx','Comunicaciones Ternium');
					$this->email->to('agarrido@ternium.com.mx, holiva@ternium.com.mx, kmarcos@ternium.com.mx, jjimeneze@ternium.com.mx, jromero@ternium.com.mx');
			        $this->email->subject('Mensaje recibido | Zintro Alum Landing Page');

			    	$data = array(
			             'nombre'=> $nombre,
			             'email'=> $email,
			             'distribuidor'=> $distribuidor,
			             'mensaje'=> $mensaje
			             );
			    	$mensajeHtml = $this->load->view('templates/email_mensajes.php',$data,TRUE);
			        $this->email->message($mensajeHtml);

			        $msg = array();
			        if($this->email->send()) {
			            $msg['status']='OK';
			            $msg['msg']='Para el equipo Ternium su opinión es muy importante, a la brevedad nos pondrémos en contacto con usted.';
			        }
			        else {
			        	$msg['status']='Error';
			            $msg['msg']='No fue posible enviar sus comentarios, intentelo nuevamente. Gracias.';
			        }
				}
	    	}
	    }
	    else {
        	$msg['status']='Error';
            $msg['msg']='Lo sentimos, no fue posible enviar tu mensaje. Intentalo nuevamente gracias.';
        }
			
		echo json_encode($msg);		
	}	
}