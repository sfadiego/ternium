<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ternium extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		

	}
	public function index()
	{
		$this->load->library('user_agent');
		
		if ($this->agent->is_mobile())
		{	
			$agent = $this->agent->mobile();
			$data['mejor_calidad'] = $this->load->view('templates/mejor_calidad_movil',"",TRUE);
			$data['contacto'] = 	 $this->load->view('templates/contacto_movil',"",TRUE);
		 	$data['caracteristica'] = $this->load->view('templates/caracteristicas_movil',"",TRUE);
			$data['sec_video'] = $this->load->view('templates/sec_video_movil',"",TRUE);
			$data['gproyectos'] = $this->load->view('templates/grandes_proyectos_movil',"",TRUE);    
		}
		else
		{
			$agent = $this->agent->browser().' '.$this->agent->version();
		    $data['caracteristica'] = $this->load->view('templates/caracteristicas',"",TRUE);
			$data['contacto'] = $this->load->view('templates/contacto',"",TRUE);
			$data['sec_video'] = $this->load->view('templates/sec_video',"",TRUE);
			$data['gproyectos'] = $this->load->view('templates/grandes_proyectos',"",TRUE);
			$data['mejor_calidad'] = $this->load->view('templates/mejor_calidad',"",TRUE);
			$data['informacion_tecnica'] = $this->load->view('templates/informacion_tecnica',"",TRUE);
			$data['guia_informacion'] = $this->load->view('templates/guia_informacion',"",TRUE);
		}

		$data['footer'] = $this->load->view('templates/footer',"",TRUE);
		$data['navbar'] = $this->load->view('templates/navbar',"",TRUE);
		$data['slider'] = $this->load->view('templates/slider',"",TRUE);
		
		$this->load->view('Home',$data);
	}
	
	public function email()
	{	

		$nombre = 	$this->input->post('contact_nombre_zintroalum');
		$correo =	$this->input->post('contact_mail_zintroalum');
		$contacto =	$this->input->post('contact_distribuidor_zintroalum');
		$mensaje =	$this->input->post('contact_mensaje_zintroalum');
		$respuesta = array();
		
		if ($nombre == "" && $correo == "" && $contacto == "" && $mensaje == "") {
			 //$respuesta = array("msg" => 'Campos faltantes','status'=>'Error');
			 $this->output->set_content_type('application/json')->set_output(json_encode(array("msg" => 'Campos faltantes','status'=>'Error')));
			 //echo $respuesta;
		}else{
			
				$to      = 'sfadiego@gmail.com';
				$subject = 'Contacto Zintro alum';
				$message =  $mensaje;
				$headers = 'From: sfadiego@gmail.com' . "\r\n".'Reply-To: sfadiego@gmail.com';

				    if (@mail($to, $subject, $message, $headers)) {
						//$respuesta = array("msg" => 'Mensaje enviado correctamente','status'=>'OK');
						$this->output->set_content_type('application/json')->set_output(json_encode(array("msg" => 'Mensaje enviado correctamente','status'=>'OK')));
				    }else{
				    	//$respuesta = array("msg" => 'Correo no enviado','status'=>'OK');
				    	$this->output->set_content_type('application/json')->set_output(json_encode(array("msg" => 'Correo no enviado','status'=>'Error')));
				    }
		    }
		
		}
}
